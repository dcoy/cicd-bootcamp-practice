FROM golang:latest AS build

WORKDIR /go/src/gitlab.com/dcoy1/cicd-bootcamp-practice
COPY http_echo.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o http_echo http_echo.go

FROM alpine:latest
WORKDIR /root/
COPY --from=build /go/src/gitlab.com/dcoy1/cicd-bootcamp-practice/http_echo .
CMD ["./http_echo"]