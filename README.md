CI/CD Bootcamp Practice

All of the code and CI scripts was borrowed from [namho-education/ci-practice](https://gitlab.com/namho-education/ci-practice) with the intent to speed along the learning process.

Stage: deploy has been removed as I will be adding my own deploy stage
