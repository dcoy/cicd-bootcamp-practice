package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoute(t *testing.T) {
	route := "/testing-route"
	request, err := http.NewRequest("GET", route, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	h := http.HandlerFunc(handler)
	h.ServeHTTP(rr, request)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}

	expected := fmt.Sprintf("URL.Path = %q\n", route)

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v, wanted %v", rr.Body.String(), expected)
	}
}
